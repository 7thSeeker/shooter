﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using System.Collections;

public class LobbyMatch : NetworkMatch
{
    MatchDesc matchDesc;

    public void SetMatchDesc(MatchDesc matchDesc)
    {
        this.matchDesc = matchDesc;
    }

    public void Connect()
    {
        JoinMatch(matchDesc.networkId, string.Empty, NetworkLobbyManager.singleton.OnMatchJoined);
    }

}
