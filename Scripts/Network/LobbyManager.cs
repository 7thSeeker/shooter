﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class LobbyManager : NetworkLobbyManager
{
    public GameObject connectionLabel;
    public GameObject offlineMenu;
    public GameObject onlineLobby;
    public GameObject serverList;
    public GameObject matchPrefab;

    public GameObject botAmandaPrefab;
    public GameObject botTheManPrefab;

    public static LobbyManager custom;


    public void ReloadLobby()
    {
        Shutdown();
        matchObjects.ForEach(x => Destroy(x));
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Destroy(gameObject);

    }


    public override void OnLobbyServerPlayersReady()
    {
        base.OnLobbyServerPlayersReady();
    }

    void Start()
    {
        custom = this;

        StartMatchMaker();
    }

    public void MatchStart()
    {
        offlineMenu.SetActive(false);
        connectionLabel.SetActive(true);

        matchMaker.CreateMatch(matchName, matchSize, false, string.Empty, OnMatchCreate);
    }

    public void MatchList()
    {
        offlineMenu.SetActive(false);
        connectionLabel.SetActive(true);

        matchMaker.ListMatches(0, 10, string.Empty, OnMatchList);
    }

    List<GameObject> matchObjects = new List<GameObject>();

    public override void OnMatchList(ListMatchResponse matchList)
    {
        base.OnMatchList(matchList);
        connectionLabel.SetActive(false);
        serverList.SetActive(true);
        matchObjects.ForEach(x => Destroy(x));
        if (matchList.success)
        {
            foreach (MatchDesc m in matchList.matches)
            {
                var mp = Instantiate(matchPrefab);
                mp.transform.SetParent(GameObject.Find("ServersList").transform);
                mp.GetComponent<LobbyMatch>().SetMatchDesc(m);
                matchObjects.Add(mp);
            }
        }
    }


    void Update()
    {
        minPlayers = numPlayers - 1;
    }

    public override void OnMatchCreate(CreateMatchResponse matchInfo)
    {
        base.OnMatchCreate(matchInfo);
        connectionLabel.SetActive(false);
    }

    public override void OnClientConnect(NetworkConnection conn)
    {
        onlineLobby.SetActive(true);
        serverList.SetActive(false);

        base.OnClientConnect(conn);
    }

    public override void OnClientSceneChanged(NetworkConnection conn)
    {
        base.OnClientSceneChanged(conn);
        GetComponent<Canvas>().enabled = SceneManager.GetActiveScene().name == "NetworkLobby"; 

    }

    public override void OnLobbyServerSceneChanged(string sceneName)
    {
        base.OnLobbyServerSceneChanged(sceneName);

        if (sceneName != "NetworkLobby" && numPlayers < 3)
        {
            if (!GameObject.FindGameObjectWithTag("Amanda"))
                NetworkServer.Spawn(Instantiate(botAmandaPrefab));

            if (!GameObject.FindGameObjectWithTag("TheMan"))
                NetworkServer.Spawn(Instantiate(botTheManPrefab));
        }
    }
}
