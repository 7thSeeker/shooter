﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class OnlinePlayer : NetworkBehaviour
{
    public static int role;

    public override void OnStartAuthority()
    {
        base.OnStartAuthority();
        if (isLocalPlayer)
            CmdCreatePlayer();
    }

    [Command]
    void CmdCreatePlayer()
    {
        var g = Instantiate(NetworkLobbyManager.singleton.spawnPrefabs[role == 2 ? 1 : 0]);
        //NetworkServer.SpawnWithClientAuthority(g, connectionToClient);
        NetworkServer.ReplacePlayerForConnection(connectionToClient, g, playerControllerId);
    }


}
