﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;

public class LobbyPlayer : NetworkLobbyPlayer
{
    public Image readyButtonImage;
    public Dropdown roleSelectionDropdown;
    public InputField nameField;

    bool ready;

    public static LobbyPlayer local;


    void Start()
    {
        if (isLocalPlayer)
            local = this;
        transform.SetParent(GameObject.Find("OnlineLobby").transform);
        readyButtonImage.GetComponent<Button>().interactable = isLocalPlayer;
        roleSelectionDropdown.interactable = isLocalPlayer;
        nameField.interactable = isLocalPlayer;
    }

    public void UpdateRole()
    {
        if (isServer)
            RpcUpdateRole(roleSelectionDropdown.value);
        else
            CmdUpdateRole(roleSelectionDropdown.value);
    }

    [Command]
    public void CmdUpdateRole(int value)
    {
        RpcUpdateRole(value);
    }

    [ClientRpc]
    void RpcUpdateRole(int role)
    {
        this.roleSelectionDropdown.value = role;
    }

    public void ReadySet()
    {
        ready = !ready;

        if (ready)
            SendReadyToBeginMessage();
        else
            SendNotReadyToBeginMessage();
    }

    void FixedUpdate()
    {
        if (isServer)
            RpcUpdateReadyButton();
        else
            CmdUpdateReadyButton();
    }

    [Command]
    void CmdUpdateReadyButton()
    {
        RpcUpdateReadyButton();
    }

    [ClientRpc]
    void RpcUpdateReadyButton()
    {
        readyButtonImage.color = readyToBegin ? Color.green : Color.red;
    }

}
