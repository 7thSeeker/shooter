﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Math = System.Math;

public static class Extensions
{
    public static T GetRandomElement<T>(this List<T> list)
    {
        return list[Random.Range(0, list.Count)];
    }

    public static Vector3 RoundVector(this Vector3 vector, int digits)
    {
        vector.x = (float)Math.Round(vector.x, digits);
        vector.y = (float)Math.Round(vector.y, digits);
        vector.z = (float)Math.Round(vector.z, digits);
        return vector;
    }

    public static Transform DeepTransformFind(this Transform root, string _name, bool createIfNoExists = false)
    {
        var result = root.Find(_name);
        if (result != null)
            return result;

        foreach (Transform t in root)
        {
            result = DeepTransformFind(t, _name);
            if (result != null)
                return result;
        }
        return createIfNoExists ? new GameObject().transform : null;
    }
}
