﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.SceneManagement;
using System.Collections;

public class LANAutoStarter : MonoBehaviour
{
#if UNITY_EDITOR
    public static bool run = true;

    IEnumerator Start()
    {
        if (run)
        {
            DontDestroyOnLoad(this.gameObject);
            run = false;
            if (SceneManager.GetActiveScene().name != "NetworkLobby")
            {
                SceneManager.LoadScene(0);
                yield return null;
                LobbyManager.singleton.StartMatchMaker();
                yield return null;
                LobbyManager.singleton.matchMaker.CreateMatch("test", 2, false, string.Empty, LobbyManager.singleton.OnMatchCreate);
                yield return new WaitForSeconds(1);
                LobbyPlayer.local.SendReadyToBeginMessage();
            }
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
#endif
}
