﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu]
public class CharacterData : ScriptableObject 
{
    public float gravity, speed, jumpSpeed, rotationSpeed, reactionRange;

    Vector3 moveDirection = new Vector3(0,0,0);
    public void Move(Transform transform, float horizontal, float vertical, bool jumping = false)
    {
        var controller = transform.GetComponent<CharacterController>();
        if (controller.isGrounded)
        {
            moveDirection = new Vector3(horizontal, 0, vertical);
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= speed;
            if (jumping)
                moveDirection.y = jumpSpeed;

        }

        moveDirection.y -= gravity * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);
    }

    public void RotateAround(Transform transform, Vector3 axis, float speed)
    {
        transform.Rotate(axis * speed * rotationSpeed);
    }
}
