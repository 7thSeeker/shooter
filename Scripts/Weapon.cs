﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[RequireComponent(typeof(AudioSource))]
public class Weapon : NetworkBehaviour
{
    public WeaponData weaponData;
    GameObject muzzleSpot;
    GameObject muzzleFlash;

    GameObject crosshair;
    [Range(1, 1000)]
    public float range = 100;

    RaycastHit aimHit;
    //RaycstHit newHit;
    Ray aimRay = new Ray();
    //Ray newRay = new Ray();

    AudioSource audioSource { get { return GetComponent<AudioSource>(); } }

    public bool performShot;
    float cd;

    bool botControlled;

    public delegate void _OnShoot();
    public static event _OnShoot OnShoot;


    void Awake()
    {
        botControlled = GetComponent<BotControlledCompanion>();
        crosshairDefaultPosition.z = range;

        muzzleSpot = transform.DeepTransformFind("muzzle_spot", true).gameObject;
        muzzleFlash = transform.DeepTransformFind("muzzle_flash", true).gameObject;
    }



    //Vector3 crosshairPosition;

    public float crosshairMoveSpeed { get { return 100 * Time.fixedDeltaTime; } }

    [ClientRpc]
    void RpcSpawnObject(SpawnableEntity ent, Vector3 pos, Quaternion rot, float lifetime)
    {
        GameObject obj;
        switch(ent)
        {
            case SpawnableEntity.ImpactEffect:
                    obj = (GameObject)Instantiate(weaponData.impactEffect, pos, rot);

                break;
            case SpawnableEntity.Decal:
                    obj = (GameObject)Instantiate(weaponData.bulletImpactDecal, pos, rot);
     
                break;

            default: obj = new GameObject(); break;
        }

        //obj.transform.parent = parent.transform;
        if (lifetime >= 0)
            Destroy(obj, lifetime);
    }

    [Command]
    void CmdSpawnObject(SpawnableEntity ent, Vector3 pos, Quaternion rot, float lifetime)
    {
        RpcSpawnObject(ent, pos, rot, lifetime);
    }

    enum SpawnableEntity
    {
        ImpactEffect,
        Decal,
    }

    [Command]
    void CmdMuzzleFlashAnimation(float cd)
    {
        RpcMuzzleFlashAnimation(cd);
    }

    [ClientRpc]
    void RpcMuzzleFlashAnimation(float cd)
    {
        muzzleFlash.SetActive(cd > 0);
        if (muzzleFlash.activeInHierarchy)
            muzzleFlash.transform.Rotate(new Vector3(Random.Range(0, 360), 0, 0));
    }

    [Command]
    void CmdShootSound()
    {
        RpcShootSound();
    }

    [ClientRpc]
    void RpcShootSound()
    {
        audioSource.PlayOneShot(weaponData.shootSounds.GetRandomElement<AudioClip>());

    }

    void Update()
    {
        if (base.isLocalPlayer)
        {
            if (Input.GetMouseButton(0))
            {
                Shoot();
            }
        }
    }

    public void Shoot()
    {
        if (cd <= 0)
        {
            CmdShootSound();
            cd = weaponData.cd;
            performShot = true;
        }
    }

    void FixedUpdate()
    {
        if (base.isLocalPlayer)
        {
            crosshairCurrentPosition = Vector3.Slerp(crosshair.transform.position, Camera.main.ViewportToWorldPoint(crosshairDefaultPosition), crosshairMoveSpeed);
            crosshair.transform.position = crosshairCurrentPosition;
            crosshair.transform.LookAt(PlayerCharacter.local.transform.position, -PlayerCharacter.local.transform.forward);
        }
 
        aimRay.origin = muzzleSpot.transform.position;
        aimRay.direction = crosshairCurrentPosition - (!botControlled ? muzzleSpot.transform.position : -transform.forward);

        Physics.Raycast(aimRay, out aimHit, range + 10);
        if (aimHit.collider)
        {
            if (base.isLocalPlayer)
                crosshair.transform.position = aimHit.point;
            if (performShot)
            {
                var rot = new Quaternion(aimHit.normal.z, aimHit.normal.y, -aimHit.normal.x, 1);

                CmdSpawnObject(SpawnableEntity.ImpactEffect, aimHit.point, rot, 1);

                if (aimHit.collider.GetType() == typeof(MeshCollider))
                    CmdSpawnObject(SpawnableEntity.Decal, aimHit.point + aimHit.normal * .001F, rot, -1);

                var t = aimHit.collider.GetComponent<Character>();
                if (t != null)
                {
                    if (isServer)
                    {
                        t.RpcApplyDamage(transform.root.gameObject, 1, aimHit);

                    }
                    else
                    {
                        t.CmdApplyDamage(transform.root.gameObject, 1, aimHit);
                    }
                }

                if (OnShoot != null)
                    OnShoot();
            }

        }


        CmdMuzzleFlashAnimation(cd);

        if (base.isLocalPlayer)
        crosshair.transform.localScale = Vector3.Lerp(crosshair.transform.localScale, Vector3.one * (.04f + cd / 2) * Vector3.Distance(Camera.main.transform.position, crosshair.transform.position), 12 * Time.fixedDeltaTime);

        performShot = false;
        cd = Mathf.Clamp(cd - Time.fixedDeltaTime, 0, 99);
    }

    Vector3 crosshairCurrentPosition;
    Vector3 crosshairDefaultPosition = new Vector3(.5F, .5F, 0);

    void Start()
    {
        if (!base.isLocalPlayer)
            return;
        crosshair = (GameObject)Instantiate(weaponData.crosshairPrefab);
        crosshair.transform.parent = transform;
    }

    void OnDisable()
    {
        Destroy(crosshair);
    }
}
