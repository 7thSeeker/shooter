﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

[System.Serializable]
[RequireComponent(typeof(NavMeshAgent))]
public class Character : NetworkBehaviour, IDamageable
{
    public int hitPointsMax;
    public int hitPointsCurrent;
    public NavMeshAgent nma { get { return GetComponent<NavMeshAgent>(); } }
    public Animator anim { get { return GetComponent<Animator>(); } }
    [HideInInspector]
    public bool isDead;

    [Command]
    public virtual void CmdApplyDamage(GameObject source, int damage, RaycastHit hitInfo)
    {
        RpcApplyDamage(source, damage, hitInfo);
    }

    [ClientRpc]
    public virtual void RpcApplyDamage(GameObject source, int damage, RaycastHit hitInfo)
    {
        if (isDead) return;

        hitPointsCurrent = Mathf.Clamp(hitPointsCurrent - damage, 0, hitPointsMax);
        TakesDamage(source);

        if (hitPointsCurrent == 0)
            Death(source);
    }

    public virtual void SetNavigationTarget(Vector3 target)
    {
        if (nma.isOnNavMesh)
            nma.destination = target;
    }

    public virtual void TakesDamage(GameObject source)
    {
        
    }

    public virtual void Death(GameObject source)
    {
        isDead = true;
        Destroy(gameObject);
    }

    public virtual void Update()
    {
        Vector3 direction = transform.InverseTransformDirection(nma.velocity);

        anim.SetFloat("Vertical", Mathf.Lerp(anim.GetFloat("Vertical"), direction.z, .95f));
        anim.SetFloat("Horizontal", Mathf.Lerp(anim.GetFloat("Horizontal"), direction.x, .95f));
    }

    public virtual void OnAnimatorMove()
    {
        anim.rootPosition = Vector3.Lerp(anim.rootPosition, nma.desiredVelocity, 1);
        //anim.rootRotation = transform.rotation;
    }




}
