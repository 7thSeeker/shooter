﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class MechStrider : Character
{
    public GameObject rocketPrefab;
    public Transform rightLauncher, leftLauncher;

    public override void Update()
    {
        base.Update();

        if (nma.hasPath && nma.remainingDistance <= nma.stoppingDistance)
        {        
            anim.SetTrigger("Attack");
            var rot = Quaternion.LookRotation(nma.destination - transform.position);
            rot.x = 0; rot.z = 0;
            transform.rotation = rot;

        }


    }

    public override void Death(GameObject source)
    {
        isDead = true;
        GetComponent<Collider>().enabled = false;
        anim.ResetTrigger("Attack");
        anim.SetTrigger("Die");
        nma.enabled = false;
        enabled = false;
    }

    public void RIGHT_RocketLaunch()
    {
        if (isServer)
        {
            RpcRightRocketLaunch();
        }
    }

    [ClientRpc]
    void RpcRightRocketLaunch()
    {
        Quaternion rot = Quaternion.LookRotation(nma.destination - rightLauncher.position);
        rot.y = transform.rotation.y;
        Instantiate(rocketPrefab, rightLauncher.position, rot);
    }

    public void LEFT_RocketLaunch()
    {
        if (isServer)
        {
            RpcLeftRocketLaunch();
        }
    }

    [ClientRpc]
    void RpcLeftRocketLaunch()
    {
        Quaternion rot = Quaternion.LookRotation(nma.destination - leftLauncher.position);
        rot.y = transform.rotation.y;
        Instantiate(rocketPrefab, leftLauncher.position, rot);
    }
}
