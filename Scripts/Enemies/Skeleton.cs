﻿using UnityEngine;
using System.Collections;

public class Skeleton : Character
{
    public override void Death(GameObject source)
    {
        isDead = true;
        GetComponent<Collider>().enabled = false;
        anim.ResetTrigger("Attack");
        anim.SetTrigger("Die");
        nma.enabled = false;
        enabled = false;
    }

    bool aboutToAttack;

    public override void Update()
    {
        base.Update();

        RaycastHit hit;
        Ray ray = new Ray(transform.position + Vector3.up / 2, transform.forward + Vector3.up / 2);

        Physics.Raycast(ray, out hit, 2);

        if (!aboutToAttack && hit.collider && hit.collider.GetComponent<PlayerCharacter>())
        {
            aboutToAttack = true;
            anim.SetTrigger("Attack");
        }
    }

    public void Slice()
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position + Vector3.up / 2, transform.forward + Vector3.up / 2);

        Physics.Raycast(ray, out hit, 2);
        if (hit.collider && hit.collider.GetComponent<PlayerCharacter>())
        {
            if (isServer)
                hit.collider.GetComponent<PlayerCharacter>().RpcApplyDamage(gameObject, 1, hit);

        }
    }

    public void Stop()
    {
        nma.Stop();
    }

    public void Resume()
    {
        aboutToAttack = false;
        nma.Resume();
    }


}
