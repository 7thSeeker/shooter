﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu]
public class WeaponData : ScriptableObject
{
    public GameObject impactEffect;
    public GameObject crosshairPrefab;
    public float cd;
    public GameObject bulletImpactDecal;
    public List<AudioClip> shootSounds;
}
