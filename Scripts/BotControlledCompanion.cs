﻿using UnityEngine;
using System.Collections;

public class BotControlledCompanion : MonoBehaviour
{
    NavMeshAgent nma;
    Animator anim;

    public string spawnerTag;

    float shootDelta = 0;

    public Weapon weapon;
    Target target;

    void Start()
    {
        transform.position = GameObject.FindGameObjectWithTag(spawnerTag).transform.position;

        nma = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();

        anim.SetBool("Run", true);

        //nma.updateRotation = false;
    }


    void Update()
    {
        if (PlayerCharacter.local)
        nma.SetDestination(PlayerCharacter.local.transform.position);

        //transform.rotation = Quaternion.LookRotation(Vector3.zero - transform.position);

        Vector3 direction = transform.InverseTransformDirection(nma.velocity);

        anim.SetFloat("Vertical", Mathf.Lerp(anim.GetFloat("Vertical"), direction.z, 10 * Time.deltaTime));
        anim.SetFloat("Horizontal", Mathf.Lerp(anim.GetFloat("Horizontal"), direction.x, 10 * Time.deltaTime));
        anim.SetLayerWeight(2, shootDelta);

        target.isLocked = target.gameObject != null && !target.gameObject.GetComponent<Character>().isDead;

        if (target.isLocked)
        {
            anim.SetFloat("AimAngleY", -Vector3.Angle(transform.root.GetComponent<Collider>().bounds.center, target.collider.bounds.center));
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(target.gameObject.transform.position - transform.position), .55F);
            weapon.Shoot();
        }

    }

    void OnAnimatorMove()
    {
        anim.rootPosition = Vector3.Lerp(anim.rootPosition, nma.desiredVelocity, 200 * Time.deltaTime);
    }

    void OnTriggerStay(Collider col)
    {
        if (!target.isLocked && col.GetComponent<Character>())
        {
            target.isLocked = true;
            target.collider = col.gameObject.GetComponent<Collider>();
            target.gameObject = col.gameObject;
        }
    }

    struct Target
    {
        public bool isLocked;
        public Collider collider;
        public GameObject gameObject;
    }
}
