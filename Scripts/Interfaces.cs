﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
public interface IDamageable
{
    [ClientRpc]
    void RpcApplyDamage(GameObject source, int damage, RaycastHit hitInfo);

    [Command]
    void CmdApplyDamage(GameObject source, int damage, RaycastHit hitInfo);
}