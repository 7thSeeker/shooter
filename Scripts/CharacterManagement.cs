﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using System.Collections;

public class CharacterManagement : NetworkBehaviour
{
    public static NetworkIdentity amandaOwner;
    public static NetworkIdentity theManOwner;
    public static NetworkInstanceId iId;

    public static CharacterManagement instance;
    void Awake()
    {
        instance = this;
    }

    [Command]
    public void CmdTakeFreeChar(NetworkIdentity id)
    {
        RpcTakeFreeChar(id);
    }

    [ClientRpc]
    void RpcTakeFreeChar(NetworkIdentity id)
    {
        if (amandaOwner == null)
        {
            amandaOwner = id;
            return;
        }

        if (theManOwner == null)
        {
            theManOwner = id;
        }
    }

    [Command]
    public void CmdSetCharacterOwner(CharacterIdentity identity, NetworkIdentity owner)
    {
        RpcSetCharacterOwner(identity, owner);
    }

    [ClientRpc]
    void RpcSetCharacterOwner(CharacterIdentity identity, NetworkIdentity owner)
    {
        switch (identity)
        {
            case CharacterIdentity.Amanda:
                amandaOwner = owner;
                break;
            case CharacterIdentity.TheMan:
                theManOwner = owner;
                break;
        }
    }

    [Command]
    public void CmdSwitchCharacterOwner()
    {
        RpcSwitchCharacterOwner();
    }

    [ClientRpc]
    void RpcSwitchCharacterOwner()
    {
        NetworkIdentity temp = amandaOwner;

        amandaOwner = theManOwner;
        theManOwner = temp;
    }

    public enum CharacterIdentity
    {
        Amanda,
        TheMan,
    }
}
