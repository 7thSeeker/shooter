﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class PlayerCharacter : NetworkBehaviour, IDamageable
{
    Rigidbody rb { get { return GetComponent<Rigidbody>(); } }
    Animator anim { get { return GetComponent<Animator>(); } }
    float h { get { return Input.GetAxis("Horizontal"); } }
    float v { get { return Input.GetAxis("Vertical"); } }

    float mx { get { return Input.GetAxis("Mouse X"); } }
    float my { get { return Input.GetAxis("Mouse Y"); } }

    float rotationSpeed = 64;
    float lookSensitivity = 64;

    public Camera alwaysRenderCam;

    public Vector3 angle;

    public NetworkAnimator netAnim;

    public static PlayerCharacter local;

    public string spawnerTag;


    public float hitPoints = 100;
    public float hitPointsMax = 100;

    [HideInInspector]
    public bool isDead;

    void Start()
    {
        col = GetComponent<Collider>();
        transform.position = GameObject.FindGameObjectWithTag(spawnerTag).transform.position;

        netAnim.enabled = true;
        
        if (!base.isLocalPlayer)
        {
            Debug.Log("no");
            anim.applyRootMotion = false;
            Destroy(alwaysRenderCam.gameObject);
            //Destroy(this);
        }
        else
        {
            local = this;
        }


        alwaysRenderCam.transform.parent = null;
    }

    void OnTriggerStay(Collider other)
    {
        if (other.GetComponent<Character>())
        {
            other.GetComponent<Character>().SetNavigationTarget(transform.position);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Character>())
        {
            other.GetComponent<Character>().SetNavigationTarget(transform.position);
        }
    }

    void Update()
    {
        if (!base.isLocalPlayer)
            return;

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            Cursor.visible = !Cursor.visible;
            Cursor.lockState = Cursor.visible ? CursorLockMode.None : CursorLockMode.Locked;
        }

        if (Input.GetKeyDown(KeyCode.Q))
            anim.SetBool("Run", !anim.GetBool("Run"));

        transform.Rotate(transform.up * mx * rotationSpeed * Time.deltaTime);


        Ray underfeetRay = new Ray(col.bounds.center, Vector3.down);
        Debug.DrawRay(underfeetRay.origin, underfeetRay.direction, Color.red);
        RaycastHit underfeetHit;
        Physics.Raycast(underfeetRay, out underfeetHit);
        var dist = Vector3.Distance(transform.position, underfeetHit.point);
        anim.SetFloat("ToTheGroundDistance", dist);
        rb.AddForce(Physics.gravity, ForceMode.Acceleration);

        //if (dist > col.bounds.center.y)
        //{
        //    transform.Translate(Physics.gravity * Time.deltaTime);
        //}
    }

    Collider col;

    void FixedUpdate()
    {
        if (!base.isLocalPlayer)
            return;

        lookSensitivity += Input.GetAxis("Mouse ScrollWheel") * 1000 * Time.deltaTime;
        rotationSpeed += Input.GetAxis("Mouse ScrollWheel") * 1000 * Time.deltaTime;

        anim.SetFloat("Vertical", v);
        anim.SetFloat("Horizontal", h);

        angle.y = Mathf.Clamp(angle.y + Mathf.Clamp(my * lookSensitivity * Time.fixedDeltaTime, -90, 90), -90, 90);

        anim.SetFloat("AimAngleY", angle.y, -90, 90);
        anim.SetFloat("AimAngleX", Mathf.Clamp(anim.GetFloat("AimAngleX") + mx * lookSensitivity * Time.deltaTime, -1, 1));

        float of = -anim.GetFloat("AimAngleY") / 90;

        var distanceToCam = 2.5F + of;
        var camHeight = 1.8F + of;
        Vector3 direction = transform.TransformDirection(new Vector3(.6F + of, camHeight, -distanceToCam));
        var camNewPosition = transform.position + direction;

        RaycastHit hit;
        Ray obstacleCheck = new Ray(transform.position, camNewPosition - transform.position);
        Physics.Raycast(obstacleCheck, out hit, distanceToCam * 1.5F, ~(1 << 10));
        //Debug.DrawRay(obstacleCheck.origin, 1.5F * distanceToCam * (camNewPosition - transform.position));
        if (hit.collider)
            camNewPosition = obstacleCheck.GetPoint(Vector3.Distance(transform.position, hit.point) - 1) + Vector3.up;

        Camera.main.transform.position = Vector3.Slerp(Camera.main.transform.position, camNewPosition, 5 * Time.deltaTime);
        Camera.main.transform.rotation = Quaternion.Slerp(Camera.main.transform.rotation, Quaternion.Euler(transform.rotation.eulerAngles + new Vector3(-anim.GetFloat("AimAngleY") / 2, 0, 0)), 10 * Time.deltaTime);

        alwaysRenderCam.transform.position = Camera.main.transform.position;
        alwaysRenderCam.transform.rotation = Camera.main.transform.rotation;

        anim.SetLayerWeight(2, Input.GetAxis("Fire1") / 1);

        CmdUpdateAnimatorLayers(anim.GetLayerWeight(2));

    }

    [ClientRpc]
    void RpcUpdateAnimatorLayers(float value)
    {
        anim.SetLayerWeight(2, value);
    }

    [Command]
    void CmdUpdateAnimatorLayers(float value)
    {
        RpcUpdateAnimatorLayers(value);
    }

    [ClientRpc]
    public void RpcApplyDamage(GameObject source, int damage, RaycastHit hitInfo)
    {
        if (isDead) return;

        isDead = true;

        hitPoints -= damage;
        if (hitPoints <= 0)
            Debug.Log(name + " is dead");
    }

    [Command]
    public void CmdApplyDamage(GameObject source, int damage, RaycastHit hitInfo)
    {
        RpcApplyDamage(source, damage, hitInfo);
    }
}
